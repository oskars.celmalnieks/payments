<?php


namespace src\Contracts\Services;

use Aura\Sql\ExtendedPdo;

interface DatabaseConnectionProviderInterface
{
    public function getConnection(): ExtendedPdo;

}