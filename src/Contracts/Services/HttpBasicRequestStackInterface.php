<?php

namespace src\Contracts\Services;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;

interface HttpBasicRequestStackInterface
{
    public function getClient(): ClientInterface;
    public function getRequestFactory(): RequestFactoryInterface;
}