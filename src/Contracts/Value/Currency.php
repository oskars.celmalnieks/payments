<?php


namespace src\Contracts\Value;


use src\Contracts\Value\ValueObjectInterface;

class Currency implements ValueObjectInterface
{
    /** @var string $baseCurrency */
    private $baseCurrency;

    /**
     * Currency constructor.
     */
    public function __construct()
    {
    }

    /**
     * @var string $baseCurrency
     * @return self
     */
    public function setCurrency(string $baseCurrency): self
    {
        $this->baseCurrency = $baseCurrency;

        return $this;
    }

    public function __toString()
    {
        return get_class($this);
    }

    /**
     * @inheritDoc
     */
    public static function getValidValues(): array
    {
        return ['EUR', 'GBP', 'USD'];
    }

    /**
     * @inheritDoc
     */
    public function getValue(): string
    {
        return $this->baseCurrency;
    }

    /**
     * @inheritDoc
     */
    public function equals(ValueObjectInterface $valueObject): bool
    {
        return $valueObject instanceof Currency;
    }
}