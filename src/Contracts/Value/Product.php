<?php


namespace src\Contracts\Value;


use src\Contracts\Value\ValueObjectInterface;

class Product implements ValueObjectInterface
{
    /** @var string $product */
    private $product;

    /**
     * Product constructor.
     */
    public function __construct()
    {
    }

    /**
     * @var string $product
     * @return self
     */
    public function setProduct(string $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function __toString()
    {
        return get_class($this);
    }

    public static function getValidValues(): array
    {
        return ['ECOM', 'POS'];
    }

    public function getValue(): string
    {
        return $this->product;
    }

    public function equals(ValueObjectInterface $valueObject): bool
    {
        return $valueObject instanceof Product;
    }

}