<?php


namespace src\Contracts\Data;


use src\Contracts\Value\Product;

class BasicFilter
{

    /** @var \DateTimeInterface $from */
    private $from;

    /** @var \DateTimeInterface $to */
    private $to;

    /** @var \DateTimeInterface $previousFrom */
    private $previousFrom;

    /** @var \DateTimeInterface $previousTo */
    private $previousTo;

    /** @var Product[] $products */
    private $products;

    /** @var string[] $merchants */
    private $merchants;

    /**
     * BasicFilter constructor.
     * @param $from
     * @param $to
     * @param $products
     */
    public function __construct($from, $to, $products)
    {
        $this->from = (new \DateTimeImmutable($from))->setTime(0,0,0);
        $this->to = (new \DateTimeImmutable($to))->setTime(23,59,59);
        $this->products = $products;

        $this->calculatePrevious();
    }

    /**
     *  calculate previouse time values
     */
    private function calculatePrevious() 
    {
        $dayNoYearFrom = $this->from->format('z') + 1;
        $dayNoYearTo = $this->to->format('z') + 1;

        $dayNoMonthFrom = $this->from->format('j');
        $dayNoMonthTo = $this->to->format('j');

        if($dayNoYearFrom == 1 && $dayNoYearTo == $this->cal_days_in_year($this->to->format('Y'))) {
            
            $this->previousFrom = $this->from->date_modify('-1year');
            $this->previousTO = $this->to->date_modify('-1year');
        
        } else if ($dayNoMonthFrom == 1 && $dayNoMonthTo == cal_days_in_month(CAL_GREGORIAN,$this->to->format('n'),$this->to->format('Y'))) {
            
            $this->previousFrom = $this->from->date_modify('-1month');
            $this->previousTO = $this->to->date_modify('-1month');
        
        } else {

            $diff = $this->to->diff($this->from)->format("%a");

            $this->previousFrom = $this->from->date_modify("-{$diff}days");
            $this->previousTO = $this->to->date_modify("-{$diff}days");
        }
    }

    /** 
     * taken from http://webnetkit.com/php-count-days-in-current-year/
     */
    private function cal_days_in_year($year){
        
        $days = 0;
        
        for($month = 1; $month <= 12; $month++){ 
            $days = $days + cal_days_in_month(CAL_GREGORIAN,$month,$year);
         }

        return $days;
    }    

}