<?php


namespace src\Contracts\Data;

use src\Contracts\Value\Currency;


class CurrencyRate
{
    /** @var Currency $targetCurrency */
    private $targetCurrency;

    /** @var string $baseCurrency */
    private $baseCurrency;

    /** @var string $exchangeRate */
    private $exchangeRate;

}