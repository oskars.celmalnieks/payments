<?php


namespace src\Exceptions;


class MissingCharsetConfigException extends \LogicException
{
    /** @var string $dsn */
    private $dsn;

    /** @var int $code */
    private $code;

    /** @var \Throwable $previous */
    private $previous;

    /**
     * MissingQueryParameterException constructor.
     * @param string $missingParam
     * @param int $code
     * @param \Throwable $previous
     */
    public function __construct(string $dsn, int $code = 0, \Throwable $previous = null)
    {
        $this->dsn = $dsn;
        $this->code = $code;
        $this->previous = $previous;

        parent::__construct($this->getCustomMessage(), $this->code, $this->previous);
    }

    private function getDsn(): string
    {
        return $this->dsn;
    }

    private function getCustomMessage()
    {
        return sprintf("Parameter charset is missing in the dns: %s", $this->getDsn());
    }
}