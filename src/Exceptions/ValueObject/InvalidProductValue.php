<?php


namespace src\Exceptions\ValueObject;


use src\Contracts\Value\Product;
use src\Contracts\Value\ValueObjectInterface;

class InvalidProductValue extends AbstractInvalidValueException
{
    protected function getValueObjectClass(): ValueObjectInterface
    {
        return new Product();
    }

}