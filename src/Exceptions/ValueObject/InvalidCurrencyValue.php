<?php


namespace src\Exceptions\ValueObject;


use src\Contracts\Value\Currency;
use src\Contracts\Value\ValueObjectInterface;

class InvalidCurrencyValue extends AbstractInvalidValueException
{
    protected function getValueObjectClass(): ValueObjectInterface
    {
        return new Currency();
    }

}