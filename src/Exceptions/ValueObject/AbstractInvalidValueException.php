<?php


namespace src\Exceptions\ValueObject;


use src\Contracts\Value\ValueObjectInterface;

abstract class AbstractInvalidValueException extends \Exception
{

    /** @var string $value */
    protected $value;

    /** @var int $code */
    protected $code;

    /** @var \Throwable $previous */
    protected $previous;

    /**
     * AbstractInvalidValueException constructor.
     * @param string $value
     * @param int $code
     * @param \Throwable $previous
     */
    public function __construct(string $value, int $code = 0, \Throwable $previous = null)
    {
        $this->value = $value;
        $this->code = $code;
        $this->previous = $previous;

        parent::__construct($this->getCustomMessage(), $code = 0, $previous);
    }

    private function getCustomMessage(): string
    {
        $message = sprintf(
            "%s is not a valid value for %s. 
            Valid values: %s; short_class_name should be a last
            component (e.g. Product, Currency) from the fully-qualified class
            name taken from getValueObjectClas s(); valid_values should be
            retrieved from ValueObjectInterface",
            $this->value,
            $this->getValueObjectClass(),
            implode(',',$this->getValueObjectClass()::getValidValues())
        );

        return $message;
    }

    abstract protected function getValueObjectClass(): ValueObjectInterface;
}