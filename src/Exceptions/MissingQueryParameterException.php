<?php


namespace src\Exceptions;


class MissingQueryParameterException extends \RuntimeException
{
    /** @var string $missingParam */
    private $missingParam;

    /** @var int $code */
    private $code;

    /** @var \Throwable $previous */
    private $previous;

    /**
     * MissingQueryParameterException constructor.
     * @param string $missingParam
     * @param int $code
     * @param \Throwable $previous
     */
    public function __construct(string $missingParam, int $code = 0, \Throwable $previous = null)
    {
        $this->missingParam = $missingParam;
        $this->code = $code;
        $this->previous = $previous;

        parent::__construct($this->getCustomMessage(), $this->code, $this->previous);
    }

    private function getMissingParam(): string
    {
        return $this->missingParam;
    }

    private function getCustomMessage()
    {
        return sprintf("%s is missing in the query", $this->getMissingParam());
    }
}