<?php


namespace src\Helpers;


use src\Contracts\Value\Currency;
use src\Contracts\Value\Product;
use src\Exceptions\MissingQueryParameterException;

class Util
{
    /**
     * @var array $params
     * @return Currency
     *
     * @throws MissingQueryParameterException
     */
    public function baseCurrencyFromRequestQuery(array $params): Currency
    {
        if (!key_exists('baseCurrency', $params)) {
            throw new MissingQueryParameterException('No Base currency parameter');
        }

        return (new Currency())->setCurrency($params['baseCurrency']);
    }

    /**
     * @var array $params
     * @return Product[]
     */
    public function productsFromRequestQuery(array $params): array
    {
        if(!key_exists('products', $params)) {
            return [];
        }

        return array_map(function ($eachProduct) {

            return (new Product())->setProduct($eachProduct);
        }, $params['products']);
    }
}