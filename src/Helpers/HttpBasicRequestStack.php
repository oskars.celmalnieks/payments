<?php


namespace src\Helpers;


use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use src\Contracts\Services\HttpBasicRequestStackInterface;

class HttpBasicRequestStack implements HttpBasicRequestStackInterface
{

    /** @var ClientInterface $client */
    private $client;

    /** @var RequestFactoryInterface $requestFactory */
    private $requestFactory;

    /**
     * HttpBasicRequestStack constructor.
     * @param ClientInterface $client
     * @param RequestFactoryInterface $requestFactory
     */
    public function __construct(ClientInterface $client, RequestFactoryInterface $requestFactory)
    {
        $this->client = $client;
        $this->requestFactory = $requestFactory;
    }


    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function getRequestFactory(): RequestFactoryInterface
    {
        return $this->requestFactory;
    }

}