<?php


namespace src\Helpers;


use Aura\Sql\DecoratedPdo;
use Aura\Sql\ExtendedPdo;
use src\Contracts\Services\DatabaseConnectionProviderInterface;

class DatabaseConnectionFromPdoProvider implements DatabaseConnectionProviderInterface
{

    /** @var ExtendedPdo $pdo */
    private $pdo;

    /**
     * DatabaseConnectionFromPdoProvider constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        if(!$pdo instanceof ExtendedPdo) {
            $this->pdo = new DecoratedPdo($pdo);
        } else {
            $this->pdo = $pdo;
        }
    }

    /** @return ExtendedPdo */
    public function getConnection(): ExtendedPdo
    {
        return $this->pdo;
    }

}