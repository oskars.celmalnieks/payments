<?php


namespace src\Helpers;


use Aura\Sql\ExtendedPdo;
use src\Contracts\Services\DatabaseConnectionProviderInterface;
use src\Exceptions\MissingCharsetConfigException;

class DatabaseConnectionProvider implements DatabaseConnectionProviderInterface
{
    /** @var string $pdoDsn */
    private $pdoDsn;

    /** @var string $username */
    private $username;

    /** @var string $password */
    private $password;

    /**
     * DatabaseConnectionProvider constructor.
     * @param string $pdoDsn
     * @param string $username
     * @param string $password
     */
    public function __construct(string $pdoDsn, string $username, string $password)
    {
        if (strpos($pdoDsn, 'charset=') === false) {
            throw new MissingCharsetConfigException('Charset not defined for PdoDsn');
        }

        $this->pdoDsn = $pdoDsn;
        $this->username = $username;
        $this->password = $password;
    }


    public function getConnection(): ExtendedPdo
    {
        return new ExtendedPdo(
            $this->pdoDsn,
            $this->username,
            $this->password
        );
    }
}